using System.Security.Cryptography;
using UnityEngine;

public static class SaveSystem
{
    private static void Save()
    {
        PlayerPrefs.Save();
    }

    public static void ClearSaves()
    {
        PlayerPrefs.DeleteAll();
    }

    public static int GetLevel() => PlayerPrefs.GetInt("Level", 1);

    public static void SaveLevel(int value)
    {
        PlayerPrefs.SetInt("Level", value);
        Save();
    }

    public static int GetLevelColorId() => PlayerPrefs.GetInt("LevelColorId", 0);

    public static void SaveLevelColorId(int value)
    {
        PlayerPrefs.SetInt("LevelColorId", value);
        Save();
    }

    public static int GetLevelProgressValue() => PlayerPrefs.GetInt("LevelProgressValue", 0);

    public static void SaveLevelProgressValue(int value)
    {
        PlayerPrefs.SetInt("LevelProgressValue", value);
        Save();
    }

    public static int GetLevelProgressMaxValue() => PlayerPrefs.GetInt("LevelProgressMaxValue", 1);

    public static void SaveLevelProgressMaxValue(int value)
    {
        PlayerPrefs.SetInt("LevelProgressMaxValue", value);
        Save();
    }

    public static int GetScore() => PlayerPrefs.GetInt("Score", 2000);

    public static void SaveScore(int value)
    {
        PlayerPrefs.SetInt("Score", value);
        Save();
    }

    public static float GetSpinnersPerSecond() => PlayerPrefs.GetFloat("SpinnersPerSecond", 0f);

    public static void SaveSpinnersPerSecond(float value)
    {
        PlayerPrefs.SetFloat("SpinnersPerSecond", value);
        Save();
    }

    public static int GetHandsAmount() => PlayerPrefs.GetInt("HandsAmount", 0);

    public static void SaveHandsAmount(int value)
    {
        PlayerPrefs.SetInt("HandsAmount", value);
        Save();
    }

}
