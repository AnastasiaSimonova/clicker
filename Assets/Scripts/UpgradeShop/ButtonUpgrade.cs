using UnityEngine;
using UnityEngine.UI;

public class ButtonUpgrade : MonoBehaviour
{
    [SerializeField] public UpgradeButtonData buttonsData;

    [SerializeField] private Image icon;
    [SerializeField] private Text title;
    [SerializeField] private Text price;
    [SerializeField] private Text level;
    [SerializeField] private Text spsAmount;

    [SerializeField] private SpinnersPerSecond spinnersPerSecond;

    private ScoreManager scoreManager;

    public int id;
    private int priceValue;
    private int upgradeLevel;



    public void Purchase()
    {
        if (CanPurchase())
        {
            scoreManager.UpdateScore(scoreManager.Score - priceValue);
            // TODO: Add particle animation
            IncreasePrice();

            AddUpgrade();
        }
        else
        {
            scoreManager.PlayNotEnoughMoneyAnimation();
        }
    }


    private void Start()
    {
        spinnersPerSecond = GameObject.Find("SpinnersPerSecond").GetComponent<SpinnersPerSecond>();

        gameObject.name = "Btn" + buttonsData.Title;

        id = buttonsData.Id;

        icon.sprite = buttonsData.Icon;
        title.text = buttonsData.Title;

        upgradeLevel = GetButtonUpgradeLevel();
        level.text = upgradeLevel.ToString();

        spsAmount.text = "+" + buttonsData.PlusSpinnersPerSecond.ToString();

        priceValue = GetButtonUpgradePrice();
        price.text = priceValue.ToString();

        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }


    private void IncreasePrice()
    {
        priceValue *= 2;
        price.text = priceValue.ToString();

        SaveButtonUpgradePrice(priceValue);
    }


    private bool CanPurchase() => (scoreManager.Score - priceValue >= 0) ? true : false;

    // Select and add purchased upgrade
    private void AddUpgrade()
    {
        AddSpinnersPerSecond(buttonsData.PlusSpinnersPerSecond);

        switch (buttonsData.Title)
        {
            case "Autoclick":
                GameObject.FindGameObjectWithTag(buttonsData.Title).GetComponent<AutoclickHandsContainer>().AddUpgrade();
                break;
            default:
                break;
        }
        
    }


    private void AddSpinnersPerSecond(float value)
    {
        spinnersPerSecond.UpdateSpinnersPerSecondAmount(value);

        UpdateUpgradeLevel();
    }


    private void UpdateUpgradeLevel()
    {
        upgradeLevel++;
        level.text = upgradeLevel.ToString();

        SaveButtonUpgradeLevel(upgradeLevel);
    }

    // TODO: Move to saves

    private int GetButtonUpgradeLevel() => PlayerPrefs.GetInt(buttonsData.Title + "UpgradeLevel", 0);

    private void SaveButtonUpgradeLevel(int value)
    {
        PlayerPrefs.SetInt(buttonsData.Title + "UpgradeLevel", value);
    }

    private int GetButtonUpgradePrice() => PlayerPrefs.GetInt(buttonsData.Title + "Price", 100);

    private void SaveButtonUpgradePrice(int value)
    {
        PlayerPrefs.SetInt(buttonsData.Title + "Price", priceValue);
    }
}
