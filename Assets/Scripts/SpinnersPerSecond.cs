using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinnersPerSecond : MonoBehaviour
{
    [SerializeField] private Text spsText;

    private float spinnersPerSecond;


    public void AddSpinnersPerSecond(float value)
    {
        spsText.text = (spinnersPerSecond + value).ToString() + " per second";
    }


    public void UpdateSpinnersPerSecondAmount(float value)
    {
        spinnersPerSecond += value;
        spsText.text = spinnersPerSecond.ToString() + " per second";

        SaveSystem.SaveSpinnersPerSecond(spinnersPerSecond);
    }


    private void Start()
    {
        spinnersPerSecond = SaveSystem.GetSpinnersPerSecond();
        spsText.text = spinnersPerSecond.ToString() + " per second";
    }
}
