using UnityEngine;
using UnityEngine.UI;

public class LevelProgress : MonoBehaviour
{
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private MainSpinner mainSpinner;
    [SerializeField] private GameObject progress;
    [SerializeField] private GameObject spinnersContainer;

    private int value;
    private int maxValue;

    private Color[] colors = new Color[] { Color.white, Color.yellow, Color.green, Color.blue, Color.magenta, Color.red };
    private int colorId;


    public void AddValue()
    {
        value += 1;
        UpdateLevelProgressValue(value);

        CheckNextLevel();
    }

    // Update progress color when level changes
    private void UpdateProgressColor()
    {
        colorId = SaveSystem.GetLevelColorId();
        progress.GetComponent<Image>().color = colors[colorId];
    }


    private void Start()
    {
        value = SaveSystem.GetLevelProgressValue();
        maxValue = SaveSystem.GetLevelProgressMaxValue();

        gameObject.GetComponent<Slider>().value = value;
        gameObject.GetComponent<Slider>().maxValue = maxValue;

        UpdateProgressColor();
    }

    // Max progress value corresponds to the number of spinners required to move to the next level
    private void IncreaseMaxValue()
    {
        // TODO: Balance it
        maxValue *= 2;
        UpdateLevelProgressMaxValue(maxValue);
    }

    private void CheckNextLevel()
    {
        if (value >= maxValue)
        {
            value = 0;
            UpdateLevelProgressValue(value);

            IncreaseMaxValue();
            mainSpinner.ChangeColor();
            UpdateProgressColor();
            levelManager.UpdateLevel();
        }
    }

    private void UpdateLevelProgressValue(int val)
    {
        gameObject.GetComponent<Slider>().value = val;
        SaveSystem.SaveLevelProgressValue(val);
    }

    private void UpdateLevelProgressMaxValue(int maxVal)
    {
        gameObject.GetComponent<Slider>().maxValue = maxVal;
        SaveSystem.SaveLevelProgressMaxValue(maxVal);
    }
}
