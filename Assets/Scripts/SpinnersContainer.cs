﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinnersContainer : MonoBehaviour
{
    [SerializeField] private GameObject spinnerPrefab;
    [SerializeField] private SpinnersPerSecond spinnersPerSecond;

    private List<GameObject> spinnersList = new List<GameObject>();

    private LevelProgress levelProgress;

    private (int, int) bordersRange = (100, 620);
    private (float, float) localScaleRange = (0.6f, 0.9f);

    private Color[] colors = new Color[] { Color.white, Color.yellow, Color.green, Color.blue, Color.magenta, Color.red };
    private int colorId;

    private const int SPINNER_POS_Y = 1620;


    public void SpawnSpinnersIfLevelUp(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            SpawnSpinner();
        }
    }


    public void SpawnSpinner()
    {
        var randomLocalScale = Random.Range(localScaleRange.Item1, localScaleRange.Item2);

        var newSpinner = Instantiate(spinnerPrefab);
        newSpinner.transform.position = new Vector3(Random.Range(bordersRange.Item1, bordersRange.Item2), SPINNER_POS_Y, 0);
        newSpinner.transform.localScale = new Vector3(randomLocalScale, randomLocalScale, 1);
        newSpinner.transform.SetParent(this.gameObject.transform);

        ApplyColor(newSpinner);

        levelProgress.AddValue();

        spinnersList.Add(newSpinner);
    }


    private void ApplyColor(GameObject spinner)
    {
        colorId = SaveSystem.GetLevelColorId();
        spinner.gameObject.GetComponent<Image>().color = colors[colorId];
    }


    private void UpdateSpinnersPerSecond()
    {
        spinnersPerSecond.AddSpinnersPerSecond(spinnersList.Count);

        spinnersList.Clear();
    }


    IEnumerator CalculateSpinners()
    {
        while (true)
        {
            UpdateSpinnersPerSecond();
            yield return new WaitForSeconds(1f);
        }
    }


    private void Start()
    {
        levelProgress = GameObject.Find("LevelProgress").GetComponent<LevelProgress>();

        StartCoroutine(CalculateSpinners());
    }
}
