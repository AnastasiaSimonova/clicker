﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class MainSpinner : MonoBehaviour
{
    private float rotationSpeed = 0f;
    private float speedStep = 20f;

    private Color[] colors = new Color[] {Color.white, Color.yellow, Color.green, Color.blue, Color.magenta, Color.red};
    private int colorId;



    public void ChangeColor()
    {
        colorId++;

        if (colorId == colors.Length) colorId = 0;

        gameObject.GetComponent<Image>().color = colors[colorId];
        SaveSystem.SaveLevelColorId(colorId);
    }


    public void PlayClickAnim()
    {
        var animDuration = 0.1f;
        this.transform.DOScale(new Vector3(0.9f, 0.9f, 1), animDuration).
            OnComplete(() => this.transform.DOScale(new Vector3(1f, 1f, 1), animDuration));
    }


    public void IncreaseSpeed(bool isTrue)
    {
        rotationSpeed += isTrue ? speedStep : ((rotationSpeed > 0) ? - speedStep : 0);
    }


    IEnumerator ReduceTime()
    {
        while (true)
        {
            IncreaseSpeed(false);
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void Start()
    {
        colorId = PlayerPrefs.GetInt("LevelColorId", 0);
        gameObject.GetComponent<Image>().color = colors[colorId];

        StartCoroutine(ReduceTime());
    }

    private void Update()
    {
        transform.Rotate(0, 0, -rotationSpeed * Time.deltaTime);
    }
}
