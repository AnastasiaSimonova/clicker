using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private Text levelText;

    public int Level { get; set; }

    private Color[] colors = new Color[] { Color.white, Color.yellow, Color.green, Color.blue, Color.magenta, Color.red };
    private int colorId;


    public void UpdateLevel()
    {
        Level++;
        levelText.text = Level.ToString();

        SaveSystem.SaveLevel(Level);

        UpdateLevelColor();
    }

    private void UpdateLevelColor()
    {
        colorId = SaveSystem.GetLevelColorId();
        gameObject.GetComponent<Image>().color = colors[colorId];
    }


    private void Start()
    {
        Level = SaveSystem.GetLevel();

        levelText.text = Level.ToString();
        UpdateLevelColor();
    }
}
