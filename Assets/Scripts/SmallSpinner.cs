﻿using UnityEngine;

public class SmallSpinner : MonoBehaviour
{
    private float speed;
    private (int, int) speedRange = (130, 250);

    private const int DESTROY_TIME = 13;


    private void Start()
    {
        speed = Random.Range(speedRange.Item1, speedRange.Item2);
    }

    private void Update()
    {
        transform.position -= new Vector3(0, speed * Time.deltaTime, 0);
        Destroy(this.gameObject, DESTROY_TIME);
    }
}
