using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoclickHandsContainer : MonoBehaviour
{
    [SerializeField] private GameObject autoclickHandPrefab;

    private const int CIRCLE_RADIUS = 280;
    private const int MAX_HANDS_PER_CIRCLE = 30;
    //private const int ROTATE_SPEED = 5;

    private int handsAmount;
    private int amount = 0;
    private float step;

    private int animHandIdx = 0; // for step-by-step hand animation

    private Vector3 centerPos;


    private void Awake()
    {
        step = Mathf.PI * 2f / MAX_HANDS_PER_CIRCLE;

        handsAmount = SaveSystem.GetHandsAmount();
    }


    private void Start()
    {
        centerPos = GameObject.Find("MainSpinner").transform.position;
        
        AddPurchasedHands();
        StartCoroutine(PlayHandAnim());
    }

    // TODO: implement circle rotation with hands
    /*private void Update()
    {
        transform.Rotate(0, 0, ROTATE_SPEED * Time.deltaTime);
    }*/


    // Add autoclick hand + 0.1 spinners per second
    public void AddUpgrade()
    {
        if (amount >= MAX_HANDS_PER_CIRCLE) return;

        var hand = Instantiate(autoclickHandPrefab);
        hand.transform.position = GetPositionInCircle(centerPos, amount);

        float angle = (Mathf.Atan2(centerPos.y - hand.transform.position.y, centerPos.x - hand.transform.position.x) - 180 / 4f) * Mathf.Rad2Deg - 35;
        hand.transform.Rotate(0, 0, angle);

        hand.transform.SetParent(this.transform);

        amount++;
        SaveSystem.SaveHandsAmount(amount);
    }

    // Called only from the Start()
    private void AddPurchasedHands()
    {
        for (int handIdx = 0; handIdx < handsAmount; handIdx++)
        {
            AddUpgrade();
        }
    }

    // Calculate hand position in circle by hand index
    private Vector3 GetPositionInCircle(Vector3 center, int handIdx)
    {
        Vector3 pos;

        pos.x = center.x + CIRCLE_RADIUS * Mathf.Sin(step * handIdx);
        pos.y = center.y + CIRCLE_RADIUS * Mathf.Cos(step * handIdx);
        pos.z = center.z;

        return pos;
    }

    // Method for tests
    public void ResetPurchasedHands()
    {
        PlayerPrefs.DeleteAll();
    }


    IEnumerator PlayHandAnim()
    {
        while (true)
        {
            SelectHandAndPlayAnim();
            yield return new WaitForSeconds(0.5f);
        }
    }

    // If the circle of hands is more than half full, choose two opposite hands for autoclick
    [Obsolete]
    private void SelectHandAndPlayAnim()
    {
        int secondHandIdx;

        if (animHandIdx >= MAX_HANDS_PER_CIRCLE / 2)
            secondHandIdx = MAX_HANDS_PER_CIRCLE / 2 + animHandIdx - MAX_HANDS_PER_CIRCLE;
        else
            secondHandIdx = MAX_HANDS_PER_CIRCLE / 2 + animHandIdx;

        int childCount = gameObject.transform.GetChildCount();
        if (childCount > animHandIdx)
            this.gameObject.transform.GetChild(animHandIdx).gameObject.GetComponent<AutoclickHand>().MoveHand();
        if (childCount > secondHandIdx)
            this.gameObject.transform.GetChild(secondHandIdx).gameObject.GetComponent<AutoclickHand>().MoveHand();

        animHandIdx++;
        if (animHandIdx > MAX_HANDS_PER_CIRCLE)
            animHandIdx = 0;
    }
}
