using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] public Animator scoreAnim;

    public int Score { get; set; }


    private void Start()
    {
        Score = SaveSystem.GetScore();
        UpdateScore(Score);
    }


    public void AddScore()
    {
        Score++;
        UpdateScore(Score);
    }


    public void UpdateScore(int score)
    {
        scoreText.text = score.ToString() + " Spinners!";

        SaveSystem.SaveScore(score);
    }


    public void PlayNotEnoughMoneyAnimation()
    {
        scoreAnim.SetBool("CanPurchase", false);
    }

    public void ChangeAnimState()
    {
        scoreAnim.SetBool("CanPurchase", true);
    }
}
