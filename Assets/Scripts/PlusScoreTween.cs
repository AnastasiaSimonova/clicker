﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class PlusScoreTween : MonoBehaviour
{
    [SerializeField] private ScoreManager scoreManager;

    [SerializeField] private Ease animEase; // current - linear

    [SerializeField] private GameObject animPrefab;
    [SerializeField] private GameObject finalPos;
    [SerializeField] private Canvas parent;

    private const int CENTER_POS_X = 360;
    private const int CENTER_POS_Y = 720;

    private const int RANDOM_STEP = 150; // TODO: rename
    

    public void AddAnim()
    {
        var anim = Instantiate(animPrefab);
        anim.transform.SetParent(this.transform);
        anim.transform.position = anim.transform.position + new Vector3(CENTER_POS_X + Random.Range(-RANDOM_STEP, RANDOM_STEP), CENTER_POS_Y, 0);

        StartMoveTween(anim);
    }

    private void StartMoveTween(GameObject anim)
    {
        var animDuration = 1.2f;
        anim.transform.DOMove(finalPos.transform.position - new Vector3(100, 0, 0), animDuration)
            .OnComplete(() => StartDisappearTween(anim))
            .SetEase(animEase);
    }

    void StartDisappearTween(GameObject anim)
    {
        scoreManager.AddScore();

        var animDuration = 0.3f;
        var text = anim.GetComponent<Text>();
        text.DOFade(0, animDuration)
            .OnComplete(() => Destroy(anim.gameObject))
            .SetEase(animEase);
    }
}
