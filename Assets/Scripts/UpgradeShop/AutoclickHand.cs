using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class AutoclickHand : MonoBehaviour
{

    private Button mainSpinnerBtn;
    private float animDuration = 0.3f;


    private void Awake()
    {
        mainSpinnerBtn = GameObject.Find("MainSpinner").GetComponent<Button>();
    }


    public void MoveHand()
    {
        this.transform.DOMove(this.transform.position + new Vector3(0, 20, 0), animDuration)
            .OnComplete(() => OnMoveHandTweenCompleted());
    }


    private void OnMoveHandTweenCompleted()
    {
        mainSpinnerBtn.onClick.Invoke();
        MoveHandBack();
    }

    private void MoveHandBack()
    {
        this.transform.DOMove(this.transform.position - new Vector3(0, 20, 0), animDuration);
    }

}
