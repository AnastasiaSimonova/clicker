using UnityEngine;
using DG.Tweening;

public class UpgradesShop : MonoBehaviour
{
    [SerializeField] private GameObject shopBoard;
    [SerializeField] private GameObject upgradeButtonPrefab;
    [SerializeField] private GameObject upgradeButtonsContainer;

    private float animDuration = 0.5f;

    private string[] buttons = {"Autoclick", "SRobot", "SFarm", "SFactory", "SCloner", "AtomicS",
                        "AlienRobot", "AlienLabs", "AlienTech", "NanoSpinner"};

    private const int START_POS_Y = -1300;
    private const int FINAL_POS_Y = 0;


    private void Start()
    {
        // TODO: Add upgrade buttons runtime - AddUpgradeButtons();
    }

    // Upgrades shop menu appearing with DoTween
    public void Appear()
    {
        this.gameObject.SetActive(true);

        shopBoard.transform.DOLocalMoveY(FINAL_POS_Y, animDuration)
            .SetEase(Ease.OutBack);
    }

    // Upgrades shop menu disappearing with DoTween
    public void Disappear()
    {
        shopBoard.transform.DOLocalMoveY(START_POS_Y, animDuration)
            .SetEase(Ease.InBack)
            .OnComplete(() => this.gameObject.SetActive(false));
    }

    // TODO: Fix and implement this method 
    private void AddUpgradeButtons()
    {
        var buttonsAmount = 10;

        for (int i = 0; i < buttonsAmount; i++)
        {
            var button = Instantiate(upgradeButtonPrefab);
            button.transform.SetParent(upgradeButtonsContainer.transform);
            button.transform.localPosition = new Vector3(0, 290 - (i * 137), 0);

            string assetPath = "ScriptableObjects/UpgradeButtonsData/" + buttons[i] + ".asset";
            button.GetComponent<ButtonUpgrade>().buttonsData = Resources.Load<UpgradeButtonData>(assetPath);

        }
    }
}
