using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New UpgradeButton", menuName = "Button Data", order = 51)]
public class UpgradeButtonData : ScriptableObject
{
    [SerializeField] private Sprite icon;

    [SerializeField] private int id;
    [SerializeField] private string title;
    [SerializeField] private int price;
    [SerializeField] private int level;
    [SerializeField] private float plusSpinnersPerSecond;
    [SerializeField] private int buttonPosY;

    public Sprite Icon => icon;
    public int Id => id;
    public string Title => title;

    public int Price
    {
        get
        {
            return price;
        }

        set
        {
            price = value;
        }
    }

    public int Level => level;
    public float PlusSpinnersPerSecond => plusSpinnersPerSecond;
    public int ButtonPosY => buttonPosY;

}
